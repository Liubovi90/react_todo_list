import React, {useState, useEffect} from 'react';
import './App.css';

//Importing Components
import Form from './components/Form';
import TodoList from './components/TodoList';

function App() {
//State stuff
const [inputText, setInputText] = useState('');
const [todos, setTodos] = useState([]);
const [status, setStatus] = useState('all');
const [filterTodos, setFilterTodos] = useState([]);

// Run Once when the app start
// useEffect( () => {
//   getLocalTodos();
// }, []);

// Use Effect - добавляет todos в filterTodos
useEffect( () => {
  const filterHandler = () => {
    switch (status) {
      case 'completed':
        setFilterTodos(todos.filter(todo => todo.completed === true));
        break;
      case 'uncompleted':
        setFilterTodos(todos.filter(todo => todo.completed === false));
        break;
       default:
         setFilterTodos(todos);
         break; 
    };    
  };

  filterHandler();
}, [todos, status]);

// Save to Local
const saveLocalTodos = () => {
     localStorage.setItem('todos', JSON.stringify(todos)); 
};
const getLocalTodos = () => { // проверяет есть ли 
  if(localStorage.getItem('todos') === null){
    localStorage.setItem('todos', JSON.stringify([]));
  } else {
    let todoLocal = JSON.parse(localStorage.getItem('todos'));
    setTodos(todoLocal);
    }
}

  return (
  <div className="App">

    <header>
      <h1>Ed's Todo List</h1>
     </header>

     <Form 
     inputText={inputText} 
     setInputText={setInputText} 
     todos={todos} 
     setTodos={setTodos}
     setStatus={setStatus}
     
     />

    <TodoList 
     todos={todos} 
     setTodos={setTodos} 
     filterTodos={filterTodos}
     />

  </div>
  );
}

export default App;
