import React from 'react';

//Import Components
import Todo from './Todo';

const TodoList = ({todos, setTodos, filterTodos}) => {
  console.log(filterTodos);
    return(
    <div className="todo-container">
           <ul className="todo-list">
             {filterTodos.map((todo) => (
               <Todo 
                  text={todo.text} 
                  key={todo.id} 
                  todos={todos}
                  todo={todo} 
                  setTodos={setTodos}
                />
             ))}
           </ul>
      </div>
    );
};

export default TodoList;


