import React from 'react';

const Form = ({inputText, setInputText, todos, setTodos, setStatus}) => {

    // Here write JS code

    const inputTextHandler = (e) => {
     console.log(e.target.value);
     setInputText(e.target.value); // берем значение инпута
    };

    const submitTodoHAndler = (e) => {
        e.preventDefault(); // отменяем перезагрузку формы
        setTodos([
            ...todos, // копируем значение todos
            {text: inputText, 
            completed: false, 
             id: Math.random()* 1000 }
        ]);
        setInputText(''); //обнуляем значение инпута
    };

    const statusHandler = (e) => { // берем значение с фильтра
     setStatus(e.target.value);
    };

  return(
    <form>
    <input  value={inputText} onChange={inputTextHandler} type="text" className="todo-input" />
    <button onClick={submitTodoHAndler} className="todo-button" type="submit">
      <i className="fas fa-plus-square"></i>
    </button>
    <div className="select">
      <select onChange={statusHandler} name="todos" className="filter-todo">
        <option value="all">All</option>
        <option value="completed">Completed</option>
        <option value="uncompleted">Uncompleted</option>
      </select>
    </div>
  </form>
  );
};

export default Form;